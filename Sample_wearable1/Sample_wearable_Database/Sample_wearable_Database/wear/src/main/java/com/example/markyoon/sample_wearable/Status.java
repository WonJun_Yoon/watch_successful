package com.example.markyoon.sample_wearable;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class Status extends LinearLayout {
    Context mContext;
    private TimerTask second;
    private TextView timer_text;
    private final Handler handler = new Handler();
    public long timer_sec=0;

    public static final int CALL_NUMBER = 1001;

    public Status(Context context) {
        super(context);

        init(context);
    }

    public Status(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    private void init(Context context) {
        mContext = context;

        // inflate XML layout
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.status, this, true);
        timer_text = (TextView) findViewById(R.id.timer);
        testStart();

    }
    public void testStart() {


        second = new TimerTask() {

            @Override
            public void run() {
                Log.i("Test", "Timer start");
                Update();
                timer_sec++;
            }
        };
        Timer timer = new Timer();
        timer.schedule(second, 0, 1000);
    }
    protected void Update() {
        Runnable updater = new Runnable() {

            public void run() {timer_text.setText("Timer = "+timer_sec + "초");
            }

        };        handler.post(updater);
    }



}

